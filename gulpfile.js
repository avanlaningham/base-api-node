const gulp = require('gulp');
const jscs = require('gulp-jscs');
const tape = require('gulp-tape');
const tapColorize = require('tap-colorize');
const filesToLint = ['server.js','routes/*.js'];
const testPath = 'tests/*.js';
const filesToWatchAndTest = ['server.js','routes/*.js',testPath];

gulp.task('lint', () => {
	return gulp.src(filesToLint)
		.pipe(jscs())
		.pipe(jscs.reporter('jscs-stylish'));
});

gulp.task('watch', function() {
  gulp.watch(filesToLint, ['lint']);
  gulp.watch(filesToWatchAndTest, ['test']);
});

gulp.task('test', function() {
  return gulp.src(testPath)
    .pipe(tape({
      reporter: tapColorize()
    }));
});

gulp.task('default', ['lint','watch']);

function clone(a) {
   return JSON.parse(JSON.stringify(a));
}
