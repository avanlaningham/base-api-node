'use strict';
var testInt = require('tape');
var tests_integration = {}
var requestObj = require('supertest');
var request;

tests_integration.runAllTests = function(){
//	console.log(request);
	testInt('integration tests', function (t) {
	request
		.get('/tests/integration/data')
		.expect('Content-Type', /json/)
		.expect(200)
		.expect(function(res2){
			t.equal(res2.body.msg,'test');
		})
		.end(function (err, res2) {
			if (err) t.error(err);
			t.end();
		});
	});
}

tests_integration.getTestObject = function(){
	return testInt;
};

tests_integration.createStream = function(res){
	var arrOut=[];
	testInt.createStream({ objectMode: true })
	.on('data', function (row) {
		arrOut.push(row);
	}).on('end', function () {
		res.json(arrOut);
	});
};

tests_integration.setRequestUrl = function(url){
	request = requestObj(url);
};

tests_integration.setRequestApp = function(app){
	request = requestObj(app);
};

module.exports = tests_integration;