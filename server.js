'use strict';
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var rfr = require('rfr');
var debug = require('debug')('base-api:server');
var http = require('http');
var apiRoutes = rfr('routes/api-routes');
var pageRoutes = rfr('routes/page-routes');
var testsIntegration = rfr('routes/tests_integration');
var config = rfr('helpers/config').config();

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//security
app.use('/api', function(req, res, next) {
  hasValidApiKey(req, res, next);
});

//routes
app.use('/tests/integration', testsIntegration); //sends all requests to api to handle
app.use('/api', apiRoutes); //sends all requests to api to handle
app.use('/', pageRoutes); //for test page(s)

//** errors and core server setup below
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

var port = process.env.PORT || '3000';

app.set('port', port);
var server = http.createServer(app);
server.listen(port);
server.on('error', function() {
  console.log('error');
  process.exit(1);
});

server.on('listening', function() {
  debug('listening');
});

var hasValidApiKey = function(req, res, next) {
  var apiKey = req.headers.apikey;
  if (config.apiKey == '' && (!apiKey || apiKey === '' || config.apiKey === apiKey)) {
    next();
  } else {
    res.status(401).send('Not authorized');
  }
};

module.exports = app;

