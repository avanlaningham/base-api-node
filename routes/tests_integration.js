'use strict';
var express = require('express');
var router = express.Router();
var rfr = require('rfr');
var testIntegration = rfr('/tests/tests_integration.js');
var app = express();

router.get('/data', function(req, res, next) {
  res.json({msg:'test'});
});

router.get('/', function(req, res, next) {
  var url = req.protocol + '://' + req.get('host'); //'http://localhost:3000';
  var request = testIntegration.setRequestUrl(url);
  testIntegration.createStream(res);
  testIntegration.runAllTests(request);
});

module.exports = router;
