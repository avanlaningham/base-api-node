'use strict';
var rfr = require('rfr');
var app = rfr('server');
var test_unit = rfr('/tests/tests_unit');
var test_integration = rfr('/tests/tests_integration');


test_integration.setRequestApp(app);
test_integration.runAllTests();

//test_unit.runAllTests();// unit must be after integration for some reason
