'use strict';
var express = require('express');
var router = express.Router();
var rfr = require('rfr');
var test_integration = rfr('/tests/tests_integration.js');
var app = express();

router.get('/data', function(req, res, next) {
	res.json({msg:"test"});
});

router.get('/', function(req, res, next) {
	var url = req.protocol + '://' + req.get('host'); //'http://localhost:3000';
	var request = test_integration.setRequestUrl(url);
	test_integration.createStream(res);
	test_integration.runAllTests(request);
});

module.exports = router;