'use strict';
var express = require('express');
var rfr = require('rfr');
var router = express.Router();


//example angular page using api
router.get('/', function(req, res, next) {
	var host =req.get('host').toString();
	var url = 'http://xxxx.wstat.io/api';
	
	if (host.indexOf('localhost') > -1){
		url = 'http://localhost:3000/api';
	}
	var model = {};
	model.url = url;
	res.render('index', {model:model});
});

module.exports = router;
