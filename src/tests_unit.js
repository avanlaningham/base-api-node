'use strict';
var express = require('express');
var router = express.Router();
var rfr = require('rfr');
var tests_unit = rfr('/tests/tests_unit');

router.get('/', function(req, res, next) {
	tests_unit.createStream(res);
	tests_unit.runAllTests();
});

module.exports = router;